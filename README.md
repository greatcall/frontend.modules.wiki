# FrontEnd.Modules
Contains customized and authored javascript modules

### How do I install it?
###### Toastr
`install-package GreatCall.FrontEnd.Modules.Toastr`
###### LazyImage
`install-package GreatCall.FrontEnd.Modules.LazyImage`
###### ScrollCrier
`install-package GreatCall.FrontEnd.Modules.ScrollCrier`
###### Analytics
`install-package GreatCall.FrontEnd.Modules.Analytics`
###### UserTimeout
`install-package GreatCall.FrontEnd.Modules.UserTimeout`
###### CrossOriginMessaging
`install-package GreatCall.FrontEnd.Modules.CrossOriginMessaging`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/frontend.modules.wiki/wiki).
